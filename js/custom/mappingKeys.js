var cKeys = {
    response: {
        slots: "slots",
        tableGames: "table_games",
        scratchCards: "scratch_cards"
    },
    categoryMap: {
        slots: "slots",
        table_games: "tableGames",
        scratch_cards: "scratchCards"
    },
    providerMap: {
        slots: {
            // netGaming: "mightygames",          //category in main tabs: provider in server response
            // dragonGaming: "dragongames",
            // dragongaming: "dragonGaming",
            mighty_games: "netGaming"
        }
    }
}