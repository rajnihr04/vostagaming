function callServer(url, obj, callback) {
    var xhttp = new XMLHttpRequest();
    var parent = this;
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            callback(this.responseText);
        }
    };
    xhttp.onerror = function () {
        console.log("error in server call on error");
    };
    xhttp.open("POST", url, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    try {
        obj = "params=" + JSON.stringify(obj);
        xhttp.send(obj);
    } catch (e) {
        console.log("error in server call ");
    }
};