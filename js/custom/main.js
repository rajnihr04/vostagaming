function main(){
}

function onLoginClick(){
    var userName = document.getElementById("user-email").value;
    var password = document.getElementById("user-password").value;
    sendLoginCall(userName, password);
}
function onLogoutClick(){
    callServer(LOGOUT_URL, {"api_key": "1pGHjL4v5APT2BkS", "token": playerData.token}, onLogoutSuccess);
}
function sendLoginCall(userName, password) {
    callServer(LOGIN_URL, {"api_key": API_KEY, "username": userName, "password": password }, onLoginSuccess);
}
function sendGetGamesCall() {
    callServer(GET_GAMES_URL, {"api_key": "1pGHjL4v5APT2BkS"}, onGetGamesSuccess);
}
function onLogoutSuccess(response){
    location.reload();
}
function onGetGamesSuccess(response){
    response = JSON.parse(response);
    response = response.result;
    var slotResponse = response[cKeys.response.slots];
    var tableGameResponse = response[cKeys.response.tableGames];
    var scratchCardResponse = response[cKeys.response.scratchCards];

    updateSlotGames(slotResponse);
    updateTableGames(tableGameResponse);
    updateScratchCardGames(scratchCardResponse);

    document.getElementById("user-email").style.display = "none";
    document.getElementById("user-password").style.display = "none";
    document.getElementById("signInButton").style.display = "none";
    document.getElementById("signOutButton").style.display = "block";
}
function onLoginSuccess(response){
    playerData = JSON.parse(response).result;
    if(playerData.token !== undefined){
        sendGetGamesCall();
    }else{
        console.log("Login Failed");
    }
}
function updateSlotGames(response){
    var elementArray = [];
    for(var i = 0; i < response.length; i++){
        for(var gameName in response[i]){
            var gameObj = response[i][gameName];
            // var logoImage = "images/thumbnails/slots/" + cKeys.providerMap.slots[gameObj.supplier] + "/" + gameName + ".jpg";
            var logoImage = gameObj.logos[1].url;
            logoImage = logoImage.replace("opus-gaming.com", "vostagames.com")
            var id = cKeys.providerMap.slots[gameObj.supplier] + gameName;
            var template = templates.slots[cKeys.providerMap.slots[gameObj.supplier]].element;
            template = template.replace("$objID$", id);
            template = template.replace("$imgSrc$", logoImage);

            var parent = templates.slots[cKeys.providerMap.slots[gameObj.supplier]].parent;
            var parentElement = document.getElementById(parent);
            // debugger;
            parentElement.innerHTML += template;

            var launch_params = {};
            for(var j = 0; j < gameObj.launch_params.length; j++){
                if(gameObj.launch_params[j].channel === getChannel()){
                    launch_params = gameObj.launch_params[j];
                }
            }

            var gameObj = {
                name: gameName,
                id: id,
                launchObj: launch_params
            }
            elementArray.push(gameObj);
        }
    }
    
    for(var i = 0; i < elementArray.length; i++){
        document.getElementById(elementArray[i].id).addEventListener("click", openGame.bind(undefined, elementArray[i]));
    }
}

function updateTableGames(response){
}
function updateScratchCardGames(response){
}
function openGame(elemObj){
    var launchLink = elemObj.launchObj.launch_url;
    launchLink = launchLink.replace("https://staging-games.mightygames.com", "https://vostagames.com/dev/ng/slots");
    launchLink = launchLink.replace("%session_id%", playerData.token);
    launchLink = launchLink.replace("%amount_type%", 1);
    launchLink = launchLink.replace("%reality_check%", 15);
    launchLink = launchLink.replace("%game_id%", elemObj.name);
    launchLink += "&gameName=" + elemObj.name;
    launchLink += "&lang=en&networkCode=ng&username=kaushik&" + playerData.username;

    window.open(launchLink, "_blank")
}
function getChannel() {
    if(isDesktop()){
        return "desktop";
    }else{
        return "mobile";
    }
};
function isDesktop(){
    return !(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini|Mobile|mobile/i.test(navigator.userAgent));
}