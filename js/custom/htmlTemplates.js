var templates = {
    slots: {
        netGaming: {
            parent: "netGaming",
            element: '<div class="game-item" id="$objID$">' +
                        '<div class="game-item-common-class">' +
                            '<img src="$imgSrc$">' +
                        '</div>' +
                        // '<div class="sl-thumb-play">' +
                        //     '<div class="sl-thumb-play-ico">' +
                        //     '</div>' +
                        //     '<div class="sl-thumb-play-bg">' +
                        //         '<img width="100%" class="lazyEffect" src="images/hover_circle.png" data-src="images/hover_circle.png">' +
                        //         '<noscript>' +
                        //             '<img width="100%" src="images/hover_circle.png">' +
                        //         '</noscript>' +
                        //     '</div>' +
                        '</div>' +
                    '</div>',
        },
        dragonGaming: {
            parent: "dragonGaming",
            element: '<div class="game-item" id="$objID$">' +
                        '<div class="game-item-common-class">' +
                            '<img src="$imgSrc$">' +
                        '</div>' +
                        '<div class="sl-thumb-play">' +
                            '<div class="sl-thumb-play-ico">' +
                            '</div>' +
                            '<div class="sl-thumb-play-bg">' +
                                '<img width="100%" class="lazyEffect" src="images/hover_circle.png" data-src="images/hover_circle.png">' +
                                '<noscript>' +
                                    '<img width="100%" src="images/hover_circle.png">' +
                                '</noscript>' +
                            '</div>' +
                        '</div>' +
                    '</div>',
        }
    }
}